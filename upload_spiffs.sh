#!/bin/sh
MKSPIFFS="./mkspiffs"
ESPTOOL="/Users/bjy/.platformio/packages/framework-arduinoespressif32\@src-537c58760dafe7fcc8a1d9bbcf00b6f6/tools/esptool.py"

$MKSPIFFS -p 256 -b 4096 -s 0x16f000 -c filesystem/ spiffs.bin
$ESPTOOL /Users/bjy/.platformio/packages/framework-arduinoespressif32\@src-537c58760dafe7fcc8a1d9bbcf00b6f6/tools/esptool.py --chip esp32 --port /dev/cu.SLAB_USBtoUART  --baud 115200 write_flash -z 0x291000 spiffs.bin 
