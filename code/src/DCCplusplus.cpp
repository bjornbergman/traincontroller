#include "configuration.h"
#include "general.h"
#include "DCCplusplus.h"
#include <Arduino.h>
namespace Trains {
    namespace DCCpp {
        void Connection::SetSpeed(TrainIdT id, SpeedT newSpeed) {}
        void Connection::SetFunction(TrainIdT id, FunctionIdT func, FunctionValueT newValue){}
        void TrainController::SetSpeed(SpeedT newSpeed)
        {
            mConnection->Send(PSTR("<t x %d %d %d>"));
        }
        void TrainController::SetFunction(FunctionIdT function, FunctionValueT newValue)
        {
            if(function <= 4)
                mConnection->Send(PSTR("<f "));
        }

        void CatalogueUpdater::Received(ArduinoJson::JsonObject const& update)
        {}
        void WebsocketConnection::DoSend(String msg)
        {
            DEBUG_MSG_P(PSTR("[DCCpp]Trying to send %s\n"), msg.c_str());
        }
    }
}
