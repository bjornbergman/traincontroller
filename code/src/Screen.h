#pragma once
#include "Buttons.h"
#include <array>
class U8G2;
namespace Screen {
	void setup();
	void loop();

	U8G2& getScreen();

	class Page;

	class PageManager {
	public:
		void addMainPage(Page* p);
		void pushPage(Page* p);
		void popPage();
		void loop();

		void handleButton(Buttons::Item item, Buttons::Event e);
	private:
		void switchPage(Page* from);
		void repaint();
		Page* topPage();
		std::array<Page*, 10> mMainPages;
		std::size_t mMainPagesSize = 0;
		std::size_t mCurrentPage = 0;
		std::array<Page*, 5> mPageStack;
		std::size_t mStackDepth = 0;
	};
	extern PageManager pages;

	//Paints whats currently on the screen:
	class Page {
	public:
		virtual void repaint(U8G2& oled) = 0;
		void repaint();
		virtual void handleButton(Buttons::Item item, Buttons::Event e);
		virtual void show(bool visible);
		virtual void loop();
	private:
		friend class PageManager;
		Page* mNext = nullptr;
	};

	class ConnectingPage : public Page {
	public:
		virtual void repaint(U8G2&oled) override;
		using Page::repaint;
		virtual void loop() override;
		//virtual void handleButton(Buttons::Item item, Buttons::Event e) override;
	};

}
