#pragma once

namespace Screen {
	class Buttons {
	public:
		enum class Event {
			Up, Down
		};
		enum class Item {
			Emergency,
			KnobPush,
			F0, F1, F2, F3,

			KnobUp,
			KnobDown,

			Item_MAX
		};

		Buttons();
		void setup();
		void loop();
		auto state(Item i) const { return mStates[static_cast<int>(i)]; }
	private:
		void pushEvent(Item i, Event e);
		static void handleIsr();

		Event mStates[static_cast<int>(Item::Item_MAX)];
	};
	extern Buttons buttons;

	bool isKnobTurn(Buttons::Item i);
	bool isPageSwitch(Buttons::Item i, Buttons::Event e);
}
