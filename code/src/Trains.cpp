#include "TrainCatalogue.h"
#include "TrainsPage.h"
#include "DCCplusplus.h"

namespace Trains {
    TrainCatalogue* catalogue;
    DCCpp::Holder* dccplusplus;
	Trains::RosterPage* rosterPage;
	Trains::CurrentTrainPage* currentTrainPage;
    void setup()
    {
        catalogue = new TrainCatalogue();
        dccplusplus = new DCCpp::Holder{*catalogue };
		rosterPage = new Trains::RosterPage{catalogue};
		currentTrainPage = new Trains::CurrentTrainPage{};
    }

    void loop()
    {
    }
}
