#include "configuration.h"
#include "general.h"
#include "Screen.h"
#include "U8g2lib.h"
#include "JustWifi.h"

U8G2_SH1106_128X64_NONAME_F_HW_I2C oled(U8G2_R0, /* reset = */ U8X8_PIN_NONE, /* scl =*/ 22, /* sda=*/21);

namespace Screen{
	PageManager pages;

	U8G2& Screen() { return oled;}
	namespace {
		ConnectingPage connectingPage;
	}
	void setup()
	{
		buttons.setup();
		//setup gpio pins for i2c0:
		oled.begin();

		pages.addMainPage(&connectingPage);
	}

	void loop()
	{
		buttons.loop();
		pages.loop();
	}

	void PageManager::addMainPage(Page* p)
	{
		if(mMainPagesSize < mMainPages.size() - 1)
			mMainPages[mMainPagesSize++] = p;
		repaint();
	}

	void PageManager::pushPage(Page* p)
	{
		if(mStackDepth >= mPageStack.size())
			return;
		auto prevPage = topPage();
		mPageStack[mStackDepth++] = p;
		switchPage(prevPage);
	}

	void PageManager::popPage()
	{
		auto prevPage = topPage();
		if(mStackDepth) {
			--mStackDepth;
			switchPage(prevPage);
		}
	}

	void PageManager::repaint()
	{
		topPage()->repaint(oled);
	}

	void PageManager::handleButton(Buttons::Item item, Buttons::Event e)
	{
		if(isPageSwitch(item, e)) {
			auto from = topPage();
			auto step = (item == Buttons::Item::KnobUp) ? -1 : 1;
			mCurrentPage = (mCurrentPage + step) % mMainPagesSize;
			Serial.printf("switching mainpage to %d item %d", mCurrentPage);
			switchPage(from);
		} else {
			topPage()->handleButton(item, e);
		}
	}

	void PageManager::switchPage(Page* from)
	{
		if(from) from->show(false);
		if(auto p = topPage()) p->show(true);
		repaint();
	}

	void PageManager::loop()
	{
		topPage()->loop();
	}

	Page* PageManager::topPage()
	{
		if(mStackDepth) {
			return mPageStack[mStackDepth - 1];
		}
		return mMainPagesSize ? mMainPages[mCurrentPage] :  nullptr;
	}

	void Page::handleButton(Buttons::Item item, Buttons::Event e)
	{}
	void Page::show(bool visible)
	{}
	void Page::repaint() { repaint(oled); }
	void Page::loop()
	{}

	void ConnectingPage::repaint(U8G2&oled)
	{
		oled.clearBuffer();
		oled.setFont(u8g2_font_4x6_tr);
		oled.setCursor(0, 5);
		int ty = 5;
		auto printfln = [&](char const* fmt, auto ...args)
		{
			oled.printf(fmt, args...);
			oled.setCursor(0, ty += 7);
		};
		if ((WiFi.getMode() & WIFI_AP) == WIFI_AP) {
			printfln("Im an AccessPoint");
			printfln("nSSID: %s", jw.getAPSSID().c_str());
			printfln("PASS: %s", getSetting("adminPass", ADMIN_PASS).c_str());
			printfln("IP: %s", WiFi.softAPIP().toString().c_str());
			printfln("MAC: %s", WiFi.softAPmacAddress().c_str());
		}

		if ((WiFi.getMode() & WIFI_STA) == WIFI_STA) {
			printfln("SSID %s\n", WiFi.SSID().c_str());
			printfln("IP   %s\n", WiFi.localIP().toString().c_str());
			printfln("MAC  %s\n", WiFi.macAddress().c_str());
			printfln("GW   %s\n", WiFi.gatewayIP().toString().c_str());
			printfln("DNS  %s\n", WiFi.dnsIP().toString().c_str());
			printfln("MASK %s\n", WiFi.subnetMask().toString().c_str());
			printfln("HOST %s\n", WiFi.getHostname());
		}
		//oled.clearBuffer();					// clear the internal memory
		/// choose a suitable font
		oled.sendBuffer();
	}
	void ConnectingPage::loop()
	{
		static int prevState = 0;
		auto currentState = WiFi.getMode();
		if(currentState != prevState) {
			repaint();
		}
	}
#if 0
		auto& s = Screen::Screen();
		s.setFont(u8g2_font_4x6_tr);	// choose a suitable font
		s.drawStr(0,58,("ip:" + WiFi.localIP().toString()).c_str());

		auto& s = Screen::Screen();
		s.setFont(u8g2_font_4x6_tr);	// choose a suitable font
		s.drawStr(0,58,("AP ip:" + WiFi.localIP().toString() + " SSID" + jw.getAPSSID()).c_str());
#endif
}
