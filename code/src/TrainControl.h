#pragma once
#include "TrainCatalogue.h"
#include <vector>
#include <Arduino.h>
//This is what can be done to a train:
namespace Trains {
    //!Used to control a train:
    class TrainController {
    public:
        virtual ~TrainController() = default;
        virtual void SetSpeed(SpeedT newSpeed) = 0;
        virtual void SetFunction(FunctionIdT function, FunctionValueT newValue) = 0;
    };

}
