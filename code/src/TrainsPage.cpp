#include "configuration.h"
#include "general.h"
#include "TrainsPage.h"
#include "Screen.h"
#include "U8g2lib.h"
namespace Trains {
	using namespace Screen;
	void RosterPage::repaint(U8G2& oled)
	{
		auto& trains = catalogue->allTrains();
		oled.clearBuffer();
		oled.setFont(u8g2_font_4x6_tr);
		oled.setCursor(0, 5);
		int ty = 5;
		auto printfln = [&](char const* fmt, auto ...args)
		{
			oled.printf(fmt, args...);
			oled.setCursor(0, ty += 7);
		};
		for(auto const& t : trains)
		{
			printfln("T: %d %s", t->Id(), t->mName.c_str(), t->mSpeed);
		}
		oled.sendBuffer();
	}
	void RosterPage::handleButton(Buttons::Item item, Buttons::Event e)
	{}

	void CurrentTrainPage::repaint(U8G2& oled)
	{
		oled.clearBuffer();
		oled.setFont(u8g2_font_4x6_tr);
		oled.setCursor(0, 5);
		oled.printf("Current train is ?");
		oled.sendBuffer();
	}
	void CurrentTrainPage::handleButton(Buttons::Item item, Buttons::Event e)
	{}
}
