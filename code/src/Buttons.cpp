#include "Buttons.h"
#include "Screen.h"
#include "driver/pcnt.h"
#include <Arduino.h>
#include <atomic>
namespace Screen {
	Buttons buttons;
	//The order should correspond to the order in Items:
	// Emergency, KnobPush, F0, F1, F2, F3,
	constexpr std::array<uint8_t,6> pins { 25, 26, 32, 33, 34, 35 };
	class PinBuffer {
		std::array<uint8_t, 16> pinBuffer;
		std::atomic<uint8_t> Read {0}, Write {0};
	public:
	 	uint8_t next(uint8_t i)
		{
			return (i + 1) % pinBuffer.size();
		}

		void push(Buttons::Item item, Buttons::Event e)
		{
			auto n = next(Write);
			if(n != Read) {
				pinBuffer[n] = pack(item, e);
				Write = n;
			}
		}

		bool tryPop(Buttons::Item& item, Buttons::Event& e)
		{
			if(Read == Write) return false;
			unpack(pinBuffer[Read], item, e);
			Read = next(Read);
			return true;
		}
		uint8_t pack(Buttons::Item i, Buttons::Event e)
		{
			return static_cast<uint8_t>(i) | (static_cast<uint8_t>(e) << 6);
		}
		void unpack(uint8_t d, Buttons::Item& item, Buttons::Event& e)
		{
			item = static_cast<Buttons::Item>(d & 0x3f);
			e = static_cast<Buttons::Event>(d >> 6);
			Serial.printf("unpacking 0x%2x to %d, %d\n", d, item, e);
		}
	};
	PinBuffer pinBuffer;

	Buttons::Buttons()
	{
		for(auto & e : mStates) e = Event::Up;
	}

	void Buttons::setup()
	{
		pcnt_config_t config;
		config.pulse_gpio_num = 27;
		config.ctrl_gpio_num = 14;
		config.lctrl_mode = PCNT_MODE_REVERSE; // or PCNT_MODE_KEEP
		config.hctrl_mode = PCNT_MODE_KEEP;
		config.pos_mode = PCNT_COUNT_INC;
		config.neg_mode = PCNT_COUNT_DIS;
		config.counter_h_lim = INT16_MAX;
		config.counter_l_lim = INT16_MIN;
		config.unit = PCNT_UNIT_0;
		config.channel = PCNT_CHANNEL_0;
		auto e = pcnt_unit_config(&config);
		pcnt_set_filter_value(config.unit, 1023); //0.5ms
		pcnt_filter_enable(config.unit);
		for(auto p : pins) {
			pinMode(p, INPUT_PULLUP);
			attachInterrupt(p, &Buttons::handleIsr, CHANGE);
		}
		pcnt_counter_resume(config.unit);
		pcnt_counter_clear(config.unit);
	}

	std::atomic<int> pin_isr_count;
	void Buttons::loop()
	{
		static int16_t prevCount = 0;
		int16_t newCount;
		pcnt_get_counter_value(PCNT_UNIT_0, &newCount);
		if(newCount != prevCount)
		{
//			Serial.printf("counters: %d %d  pin_isr: %d\n", prevCount, newCount, pin_isr_count.load());
			bool isUp = newCount > prevCount;
			pushEvent(isUp ? Item::KnobUp : Item::KnobDown, Event::Down);
			if(newCount > 10000 || newCount < -10000)
			{
				//Serial.printf("clearing!counters: %d %d  pin_isr: %d\n", prevCount, newCount, pin_isr_count.load());
				pcnt_counter_clear(PCNT_UNIT_0);
				prevCount = newCount = 0;
			}
			else
			{
				prevCount= newCount;
			}
		}
		Buttons::Item i;
		Buttons::Event e;
		while(pinBuffer.tryPop(i,e)) pushEvent(i, e);
	}

	void IRAM_ATTR Buttons::handleIsr()
	{
		static std::array<int, pins.size()> prevState{0};
		for(size_t i = 0; i < pins.size(); ++i) {
			auto current = digitalRead(pins[i]);
			if(current != prevState[i])
			{
				pinBuffer.push(static_cast<Buttons::Item>(i),
					current == 0 ? Buttons::Event::Up : Buttons::Event::Down);
				prevState[i] = current;
			}
		}
	}

	void Buttons::pushEvent(Item i, Event e)
	{
		mStates[static_cast<int>(i)] = e;
		pages.handleButton(i, e);
	}

	bool isKnobTurn(Buttons::Item i)
	{
		return i == Buttons::Item::KnobUp || i == Buttons::Item::KnobDown;
	}
	bool isPageSwitch(Buttons::Item i, Buttons::Event e)
	{
		Serial.printf("checking if %d, %d is a page switch. knob push = %d\n",
			i, e, buttons.state(Buttons::Item::KnobPush));
		return isKnobTurn(i) && buttons.state(Buttons::Item::KnobPush) == Buttons::Event::Down;
	}

}
