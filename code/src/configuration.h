#define SPIFFS_SUPPORT 1
#define WEB_EMBEDDED 0
#define SERIAL_BAUDRATE 115200

#define EEPROM_SIZE             4096


#define APP_NAME                "TrainController"
#define APP_VERSION             "0.1"
#define APP_AUTHOR              "bjornbergman@gmail.com"
#define APP_WEBSITE             "http://"
