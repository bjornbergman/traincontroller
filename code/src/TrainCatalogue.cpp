#include "TrainCatalogue.h"

#include <algorithm>

namespace Trains {

    namespace {
        auto getId(TrainIdT i) { return i;}
        auto getId(std::unique_ptr<TrainStatus> const& ptr) { return ptr->Id(); }
        struct CmpId {
            template<typename L, typename R>
            auto operator()(L const& lhs, R const& rhs)
            {
                return getId(lhs) < getId(rhs);
            }
        };
        template<typename Cont>
        auto lower_bound(Cont& trains, TrainIdT id)
        { return std::lower_bound(trains.begin(), trains.end(), id, CmpId{}); }
    }
    TrainStatus* TrainCatalogue::get(TrainIdT train)
    {
        auto it = lower_bound(mTrains, train);
        if(it != mTrains.end() && (*it)->Id() == train)
            return it->get();
        else
            return nullptr;
    }
    TrainStatus* TrainCatalogue::addIfNonExistant(TrainIdT train)
    {
        auto it = lower_bound(mTrains, train);
        if(it != mTrains.end() && (*it)->Id() == train)
            return it->get();
        else
        {if(it != mTrains.end()) ++it; // it is now the upper bound.
            auto theNew = mTrains.insert(it, std::make_unique<TrainStatus>(train));
            return theNew->get();
        }
    }
    //Move offset steps in the list of trains.
    TrainStatus* TrainCatalogue::next(TrainStatus* from, std::ptrdiff_t offset)
    {
        if(mTrains.empty())
            return nullptr;
        auto lb =(from != nullptr) ? lower_bound(mTrains, from->mId) : mTrains.begin();
        //given that mTrains contains from, lb is from.
        if(offset  < 0 ) offset = mTrains.size() + offset;
        auto index = lb - mTrains.begin();
        return (mTrains.begin() + (index + offset) % mTrains.size())->get();
    }

    TrainStatus& TrainCatalogue::Update(TrainIdT train)
    {
        auto t = addIfNonExistant(train);
        t->mUpdateNumber++;
        return *t;
    }
}
