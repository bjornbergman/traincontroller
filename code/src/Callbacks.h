#pragma once
#include<algorithm>
#include <functional>
#include <memory>
#include <vector>
//callback list:
namespace Callbacks {
    template<typename ... Args>
    class Collection;

    namespace detail {
        template<typename ... Args>
        class Handle {
        public:
            using Func = std::function<void(Args...)>;
            using Coll = Collection<Args...>;
            Handle(Func f, Coll* c) : mFunc {std::move(f)}, mCollection{c}{}
            ~Handle() {
                if(mCollection) mCollection->Remove(this);
            }
            Func mFunc;
            Coll* mCollection;
        };
    }

    template<typename ... Args>
    class Collection{
    public:
        using HandleImpl = detail::Handle<Args...>;
        using Handle = std::unique_ptr<HandleImpl>;
        using Func = typename HandleImpl::Func;
        Handle Add(Func f)
        {
            Handle res { new HandleImpl(std::move(f), this)};
            mHandlers.push_back(res.get());
            return res;
        }
        template<typename ...As>
        void Call(As ... as)
        {
            for(auto h : mHandlers)
            {
                if(h)
                    h->mFunc(std::forward<As>(as)...);
            }
        }
    private:
        friend HandleImpl;
        void Remove(HandleImpl* h)
        {
            auto it = std::remove(mHandlers.begin(), mHandlers.end(), h);
            mHandlers.erase(it);
        }
        std::vector<HandleImpl*> mHandlers;
    };
}
