#pragma once
#include "Callbacks.h"
#include "TrainControl.h"
#include "ArduinoJson.h"
#include <AsyncWebSocket.h>

namespace Trains {
    class TrainCatalogue;
namespace DCCpp {

    //!This is the (TCP) connection to DCC++
    class Connection {
    public:
        using JsonHandlers = Callbacks::Collection<ArduinoJson::JsonObject const&>;

        void Send(PGM_P format, ...);
        void Send(String msg);
        using ReceiverHandle = JsonHandlers::Handle;
        ReceiverHandle Register(JsonHandlers::Func h) { return mJsonHandlers.Add(std::move(h));}
    protected:
        virtual void SetSpeed(TrainIdT id, SpeedT newSpeed);
        virtual void SetFunction(TrainIdT id, FunctionIdT func, FunctionValueT newValue);
        //! transmit a message to dccpp
        virtual void DoSend(String msg) = 0;
        //Called by the transport whenn a whole line is received.
        void Receiver(String msg);
    private:
        JsonHandlers mJsonHandlers;
    };

    //!Listens to a connection, and updates the train catalogue with most recent speeds etc.
    class CatalogueUpdater {
        Connection& mConnection;
        Trains::TrainCatalogue& mCatalogue;
        Connection::ReceiverHandle mHandle;
    public:
        CatalogueUpdater(Connection& con, Trains::TrainCatalogue& catalogue)
        : mConnection{ con }
        , mCatalogue{ catalogue }
        , mHandle { mConnection.Register([this](auto const& m){Received(m);}) }
        {}

    private:
        void Received(ArduinoJson::JsonObject const& update);
    };

    class TrainController : public Trains::TrainController {
        Connection* mConnection = nullptr;
        Trains::TrainIdT mId = kInvalidTrain;
    public:
        TrainController(Connection& c, Trains::TrainIdT id) : mConnection {&c}, mId{id}{}

        virtual void SetSpeed(SpeedT newSpeed) override;
        virtual void SetFunction(FunctionIdT function, FunctionValueT newValue) override;
    };

    class WebsocketConnection : public Connection {
    public:
        //! transmit a message to dccpp
        virtual void DoSend(String msg) override;
    };
    class Holder {
        TrainCatalogue& mCatalogue;

        WebsocketConnection mConnection;
        CatalogueUpdater mUpdater;
    public:
        Holder(TrainCatalogue& cat)
        : mCatalogue{cat}
        , mConnection {}
        , mUpdater { mConnection, mCatalogue}
        {}
    };
}}
