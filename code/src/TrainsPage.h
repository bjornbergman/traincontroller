#pragma once
#include "Screen.h"
#include "TrainCatalogue.h"

namespace Trains {
	class TrainCatalogue;
	class RosterPage : public Screen::Page
	{
		TrainCatalogue* catalogue;
		TrainIdT selected = kInvalidTrain;
	public:
		RosterPage(TrainCatalogue* c) : catalogue{c}{ Screen::pages.addMainPage(this); };
		virtual void repaint(U8G2& oled) override;
		virtual void handleButton(Screen::Buttons::Item item, Screen::Buttons::Event e) override;
	};

	class CurrentTrainPage : public Screen::Page {
	public:
		CurrentTrainPage() { Screen::pages.addMainPage(this);}
		virtual void repaint(U8G2& oled) override;
		virtual void handleButton(Screen::Buttons::Item item, Screen::Buttons::Event e) override;
	};
}
