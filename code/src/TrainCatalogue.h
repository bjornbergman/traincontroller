#pragma once
#include <Arduino.h>
#include <vector>
#include <memory>

namespace Trains {
    using TrainIdT = int;
    constexpr TrainIdT kInvalidTrain = -1;
    using SpeedT = int;
    using FunctionIdT = int;
    using FunctionValueT = bool;
    using FunctionValueSetT = std::vector<bool>;
    struct FunctionInfo {
        String name;
    };
    //!Used to query train status
    class TrainCatalogue;
    class TrainStatus {
        TrainIdT mId;
    public:
        TrainStatus(TrainIdT id) : mId{id}{}
        auto Id() const { return mId; }
        String mName;
        std::vector<FunctionInfo> mFunctions;
        SpeedT mSpeed;
        FunctionValueSetT mFunctionValues;
        int mUpdateNumber = 0;  //Used to keep track of updates.
        friend class TrainCatalogue;
    };

    class TrainCatalogue {
        //Ordered by trainid.
        std::vector<std::unique_ptr<TrainStatus>> mTrains;
    public:
		auto& allTrains() { return mTrains; }
        TrainStatus* get(TrainIdT train);
        TrainStatus* addIfNonExistant(TrainIdT train);
        //Move offset steps in the list of trains.
        TrainStatus* next(TrainStatus* from, std::ptrdiff_t offset);

        TrainStatus& Update(TrainIdT train);
    };
}
